#include "audio.h"

namespace arkanoid
{
	namespace audio
	{
		const float cityAmbianceVolume = 0.1f;
		const float buttonsVolume = 0.3f;
		const float bricksVolume = 0.2f;
		const float menuMusicVolume = 0.2f;
		const float mutedVolume = 0.0f;

		bool muted = false;

		Music menuMusic = LoadMusicStream("res/assets/music/menu_music.mp3");
		Music gameplayMusic = LoadMusicStream("res/assets/music/gameplay_music.mp3");
		Music cityAmbiance = LoadMusicStream("res/assets/sfx/city_ambiance.mp3");
		Sound buttonsfx = LoadSound("res/assets/sfx/button_sfx.mp3");
		Sound bricksfx = LoadSound("res/assets/sfx/brick_break.mp3");

		void setNormalVolumes()
		{
			SetMusicVolume(menuMusic, menuMusicVolume);
			SetMusicVolume(gameplayMusic, gameplayVolume);
			SetMusicVolume(cityAmbiance, cityAmbianceVolume);
			SetSoundVolume(buttonsfx, buttonsVolume);
			SetSoundVolume(bricksfx, bricksVolume);
		}
		void muteInput()
		{
			if (IsKeyPressed(KEY_L))
			{
				if (!muted)
				{
					muted = true;
				}
				else
				{
					muted = false;
				}
			}
		}
		void muteButtonUpdate()
		{
			if (muted)
			{
				SetMusicVolume(menuMusic, mutedVolume);
				SetMusicVolume(gameplayMusic, mutedVolume);
				SetMusicVolume(cityAmbiance, mutedVolume);
				SetSoundVolume(buttonsfx, mutedVolume);
				SetSoundVolume(bricksfx, mutedVolume);
			}
			else
			{
				setNormalVolumes();
			}
		}
		void drawMuteButton()
		{
			if (muted)
			{
				DrawTexture(images::shortButton, screens::middleScreenW - 60, screens::middleScreenH + 150, WHITE);
				DrawText("L Unmute", middleScreenW - 46, screens::middleScreenH + 160, minFontSize, WHITE);
			}
			else
			{
				DrawTexture(images::shortButton, screens::middleScreenW - 60, screens::middleScreenH + 150, WHITE);
				DrawText("L  Mute", middleScreenW - 46, screens::middleScreenH + 160, minFontSize, WHITE);
			}
		}
	}
}