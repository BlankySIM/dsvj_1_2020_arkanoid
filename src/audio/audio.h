#ifndef AUDIO_H
#define AUDIO_H
#include "raylib.h"
#include "images/images.h"

namespace arkanoid
{
	namespace audio
	{
		const float gameplayVolume = 0.3f;
		const float gameplayPauseVolume = 0.1f;

		extern bool muted;

		extern Music menuMusic;
		extern Music gameplayMusic;
		extern Music cityAmbiance;
		extern Sound buttonsfx;
		extern Sound bricksfx;

		void setNormalVolumes();
		void muteInput();
		void muteButtonUpdate();
		void drawMuteButton();
	}
}

#endif
