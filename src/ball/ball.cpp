#include "ball.h"

namespace arkanoid
{
	namespace ball
	{
		const float collisionPointRadius = 3.0f;
		const float ballDefaultRadius = 8.0f;

		void init()
		{
			arkanoidBall.ballPosition = { static_cast<float>(middleScreenW), static_cast<float>(screenHeight) - 50 };
			arkanoidBall.ballSpeed = { 0, initialBallSpeed };
			arkanoidBall.ballRadius = ballDefaultRadius;
		}
		void ballbehavior()
		{
			arkanoidBall.ballPosition.x += arkanoidBall.ballSpeed.x *GetFrameTime();
			arkanoidBall.ballPosition.y += arkanoidBall.ballSpeed.y *GetFrameTime();

			if (arkanoidBall.ballPosition.x - arkanoidBall.ballRadius <= screenZero && arkanoidBall.ballSpeed.x < 0)
			{
				arkanoidBall.ballSpeed.x *= -1.0f;
			}
			else if (arkanoidBall.ballPosition.x + arkanoidBall.ballRadius >= screenWidth && arkanoidBall.ballSpeed.x > 0)
			{
				arkanoidBall.ballSpeed.x *= -1.0f;
			}
			else if (arkanoidBall.ballPosition.y - arkanoidBall.ballRadius <= screenZero && arkanoidBall.ballSpeed.y < 0)
			{
				arkanoidBall.ballSpeed.y *= -1.0f;
			}
		}	
		void draw()
		{
			DrawTexture(images::ball, static_cast<int>(arkanoidBall.ballPosition.x - arkanoidBall.ballRadius), static_cast<int>(arkanoidBall.ballPosition.y - arkanoidBall.ballRadius), WHITE);
			#if DEBUG
			DrawCircleLines(static_cast<int>(ball::arkanoidBall.ballPosition.x), static_cast<int>(ball::arkanoidBall.ballPosition.y), ball::arkanoidBall.ballRadius, GREEN);
			DrawCircle(static_cast<int>(arkanoidBall.ballPosition.x - arkanoidBall.ballRadius), static_cast<int>(arkanoidBall.ballPosition.y), collisionPointRadius, GREEN);
			DrawCircle(static_cast<int>(arkanoidBall.ballPosition.x + arkanoidBall.ballRadius), static_cast<int>(arkanoidBall.ballPosition.y), collisionPointRadius, GREEN);
			DrawCircle(static_cast<int>(arkanoidBall.ballPosition.x), static_cast<int>(arkanoidBall.ballPosition.y - arkanoidBall.ballRadius), collisionPointRadius, GREEN);
			DrawCircle(static_cast<int>(arkanoidBall.ballPosition.x), static_cast<int>(arkanoidBall.ballPosition.y + arkanoidBall.ballRadius), collisionPointRadius, GREEN);
			#endif
		}

		BALL arkanoidBall = { 0 };
	}
}