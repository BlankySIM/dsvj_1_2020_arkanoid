#ifndef BALL_H
#define BALL_H
#include "raylib.h"
#include "screens/screens.h"
#include "images/images.h"

using namespace arkanoid::screens;

namespace arkanoid
{
	namespace ball
	{
		const float initialBallSpeed = -400.0f;

		struct BALL
		{
			Vector2 ballPosition;
			Vector2 ballSpeed;
			float ballRadius;
		};

		void init();
		void ballbehavior();
		void draw();

		extern BALL arkanoidBall;
	}
}
#endif
