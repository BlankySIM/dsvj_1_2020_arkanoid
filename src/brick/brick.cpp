#include "brick.h"

namespace arkanoid
{
	namespace brick
	{
		int bricksToWin = 0;
		const int twoLines = 2;
		const int threeLines = 3;
		const int fourLines = 4;
		const int brickSizeX = screens::screenWidth/brickscols;
		const int brickSizeY = 50;
		const int halfbrickX = brickSizeX/2;
		const int halfbrickY = brickSizeY/2;
		const int firstLinePos = brickSizeY + 30;
		const int easyLevels = 6;
		const int mediumLevels = 11;
		const int hardLevels = 16;
		const int firstBrickX = 1;
		const int secondBrickX = brickscols - 2;
		const int randomLevels = 2;
		const int columnsLevels = 4;
		const int brickBreakPoints = 10;
		const int easyAmount = 4;
		const int mediumAmount = 6;
		const int hardAmount = 8;
		const int maxAmount = 10;
		int levelCounter = firstLevel;

		static void generateUnbreakableCols()
		{
			int unbreakableCol;
			int unbreakableCol2;

			do
			{
				unbreakableCol = GetRandomValue(0, brickscols - 1);
				unbreakableCol2 = GetRandomValue(0, brickscols - 1);
			} while (unbreakableCol == unbreakableCol2);

			for (int i = 0; i < bricksMaxLines; i++)
			{
				for (int j = 0; j < brickscols; j++)
				{
					if (j == unbreakableCol || j == unbreakableCol2)
					{
						if (!bricks[j][i].destroyed)
						{
							bricks[j][i].unbrakable = true;
							bricksToWin--;
						}
					}
				}
			}
		}
		static void generateUnbreakableV()
		{
			int line = 0;
			int firstBrickPos = firstBrickX;
			int secondBrickPos = secondBrickX;

			for (int i = 0; i < bricksMaxLines; i++)
			{
				for (int j = 0; j < brickscols; j++)
				{
					if (!bricks[j][i].destroyed && i == line && (j == firstBrickPos || j == secondBrickPos))
					{
							bricks[j][i].unbrakable = true;
							bricksToWin--;
					}
				}
				line++;
				firstBrickPos++;
				secondBrickPos--;
			}
		}
		static void randomGeneration()
		{
			int amountOfBricks;
			int specialBrickX;
			int specialBrickY;

			if (currentLevel<easyLevels)
			{
				amountOfBricks = easyAmount;
			}
			else if (currentLevel < mediumLevels)
			{
				amountOfBricks = mediumAmount;
			}
			else if (currentLevel < hardLevels)
			{
				amountOfBricks = hardAmount;
			}
			else
			{
				amountOfBricks = maxAmount;
			}

			for (int x = 0; x < amountOfBricks; x++)
			{
				do
				{
					specialBrickX = GetRandomValue(0, brickscols - 1);
					specialBrickY = GetRandomValue(0, bricksMaxLines - 1);
				} while (bricks[specialBrickX][specialBrickY].destroyed
						|| bricks[specialBrickX][specialBrickY].unbrakable);

				for (int i = 0; i < bricksMaxLines; i++)
				{
					for (int j = 0; j < brickscols; j++)
					{
						if (specialBrickX == j && specialBrickY == i)
						{
							if (x % 2 == 0)
							{
								bricks[j][i].unbrakable = true;							
							}
							else
							{
								bricks[j][i].destroyed = true;
							}
							bricksToWin--;
						}
					}
				}
			}
		}
		void init()
		{
			bricksToWin = 0;

			for (int i = 0; i < bricksMaxLines; i++)
			{
				for (int j = 0; j < brickscols; j++)
				{
						bricks[j][i].position.x = static_cast<float>(j * brickSizeX + halfbrickX);
						bricks[j][i].position.y = static_cast<float>(i * brickSizeY + firstLinePos);

						if (currentLevel < easyLevels)
						{
							if (i < twoLines)
							{
								bricks[j][i].destroyed = false;
								bricks[j][i].unbrakable = false;
								bricksToWin++;
							}
							else
							{
								bricks[j][i].destroyed = true;
								bricks[j][i].unbrakable = false;
							}
						}
						else if (currentLevel < mediumLevels)
						{
							if (i < threeLines)
							{
								bricks[j][i].destroyed = false;
								bricks[j][i].unbrakable = false;
								bricksToWin++;
							}
							else
							{
								bricks[j][i].destroyed = true;
								bricks[j][i].unbrakable = false;
							}
						}
						else if (currentLevel < hardLevels)
						{
							if (i < fourLines)
							{
								bricks[j][i].destroyed = false;
								bricks[j][i].unbrakable = false;
								bricksToWin++;
							}
							else
							{
								bricks[j][i].destroyed = true;
								bricks[j][i].unbrakable = false;
							}
						}
						else
						{
							bricks[j][i].destroyed = false;
							bricks[j][i].unbrakable = false;
							bricksToWin++;
						}
				}
			}
			if (levelCounter <= randomLevels)
			{
				randomGeneration();
				levelCounter++;
			}
			else if (levelCounter <= columnsLevels)
			{
				generateUnbreakableCols();
				levelCounter++;
			}
			else
			{
				generateUnbreakableV();
				levelCounter = firstLevel;
			}
		}
		void draw()
		{
			for (int i = 0; i < bricksMaxLines; i++)
			{
				for (int j = 0; j < brickscols; j++)
				{
					if (!bricks[j][i].destroyed)
					{
						if (!bricks[j][i].unbrakable)
						{
							DrawTexture(images::brick, static_cast<int>(bricks[j][i].position.x - halfbrickX), static_cast<int>(bricks[j][i].position.y - halfbrickY), WHITE);
						}
						else
						{
							DrawTexture(images::unbreakableBrick, static_cast<int>(bricks[j][i].position.x - halfbrickX), static_cast<int>(bricks[j][i].position.y - halfbrickY), WHITE);
						}
						#if DEBUG
						DrawRectangleLines(static_cast<int>(bricks[j][i].position.x - halfbrickX), static_cast<int>(bricks[j][i].position.y - halfbrickY), brickSizeX, brickSizeY, GREEN);
						#endif
					}
				}
			}
		}
		void ballCollision()
		{
			for (int i = 0; i < bricksMaxLines; i++)
			{
				for (int j = 0; j < brickscols; j++)
				{
					if (!bricks[j][i].destroyed)
					{
						if (ball::arkanoidBall.ballPosition.y - ball::arkanoidBall.ballRadius <= bricks[j][i].position.y + halfbrickY
							&& ball::arkanoidBall.ballPosition.y - ball::arkanoidBall.ballRadius >= bricks[j][i].position.y
							&& ball::arkanoidBall.ballPosition.x >= bricks[j][i].position.x -halfbrickX
							&& ball::arkanoidBall.ballPosition.x <= bricks[j][i].position.x + halfbrickX && ball::arkanoidBall.ballSpeed.y < 0)
						{
							if (!bricks[j][i].unbrakable)
							{
								bricks[j][i].destroyed = true;
								bricksToWin--;
								player::arkanoidPlayer.points += brickBreakPoints;
								PlaySound(audio::bricksfx);
							}
							ball::arkanoidBall.ballSpeed.y *= -1.0f;
						}
						else if (ball::arkanoidBall.ballPosition.y + ball::arkanoidBall.ballRadius >= bricks[j][i].position.y - halfbrickY
							&& ball::arkanoidBall.ballPosition.y + ball::arkanoidBall.ballRadius <= bricks[j][i].position.y
							&& ball::arkanoidBall.ballPosition.x >= bricks[j][i].position.x - halfbrickX
							&& ball::arkanoidBall.ballPosition.x <= bricks[j][i].position.x + halfbrickX && ball::arkanoidBall.ballSpeed.y > 0)
						{
							if (!bricks[j][i].unbrakable)
							{
								bricks[j][i].destroyed = true;
								bricksToWin--;
								player::arkanoidPlayer.points += brickBreakPoints;
								PlaySound(audio::bricksfx);
							}
							ball::arkanoidBall.ballSpeed.y *= -1.0f;
						}
						else if (ball::arkanoidBall.ballPosition.x + ball::arkanoidBall.ballRadius >= bricks[j][i].position.x - halfbrickX
							&& ball::arkanoidBall.ballPosition.x + ball::arkanoidBall.ballRadius <= bricks[j][i].position.x
							&& ball::arkanoidBall.ballPosition.y >= bricks[j][i].position.y - halfbrickY
							&& ball::arkanoidBall.ballPosition.y <= bricks[j][i].position.y + halfbrickY && ball::arkanoidBall.ballSpeed.x > 0)
						{
							if (!bricks[j][i].unbrakable)
							{
								bricks[j][i].destroyed = true;
								bricksToWin--;
								player::arkanoidPlayer.points += brickBreakPoints;
								PlaySound(audio::bricksfx);
							}
							ball::arkanoidBall.ballSpeed.x *= -1.0f;
						}
						else if (ball::arkanoidBall.ballPosition.x - ball::arkanoidBall.ballRadius <= bricks[j][i].position.x + halfbrickX
							&& ball::arkanoidBall.ballPosition.x - ball::arkanoidBall.ballRadius >= bricks[j][i].position.x
							&& ball::arkanoidBall.ballPosition.y >= bricks[j][i].position.y - halfbrickY
							&& ball::arkanoidBall.ballPosition.y <= bricks[j][i].position.y + halfbrickY && ball::arkanoidBall.ballSpeed.x < 0)
						{
							if (!bricks[j][i].unbrakable)
							{
								bricks[j][i].destroyed = true;
								bricksToWin--;
								player::arkanoidPlayer.points += brickBreakPoints;
								PlaySound(audio::bricksfx);
							}
							ball::arkanoidBall.ballSpeed.x *= -1.0f;
						}
					}
				}
			}
		}
		void deinit()
		{

		}

		BRICK bricks[brick::brickscols][brick::bricksMaxLines] = { 0 };
	}
}