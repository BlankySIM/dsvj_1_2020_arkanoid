#ifndef BRICK_H
#define BRICK_H
#include "raylib.h"
#include "player/player.h"
#include "audio/audio.h"
#include "images/images.h"
#include "ball/ball.h"

namespace arkanoid
{
	namespace brick
	{
		extern int bricksToWin;
		extern int levelCounter;
		const int bricksMaxLines = 5;
		const int brickscols = 10;	

		struct BRICK
		{
			Vector2 position;
			bool destroyed;
			bool unbrakable;
		};

		void init();
		void draw();
		void ballCollision();
		void deinit();

		extern BRICK bricks[brick::brickscols][brick::bricksMaxLines];
	}
}
#endif
