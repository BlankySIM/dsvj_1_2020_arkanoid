#include "game.h"

namespace arkanoid
{
	static void init()
	{
		InitWindow(static_cast<int>(screenWidth), static_cast<int>(screenHeight), "Arkanoid");
		InitAudioDevice();
		menu::init();
		gameplay::init();
		credits::init();
		instructions::init();
		results::init();
		SetTargetFPS(60);
	}
	static void inputs()
	{
		switch (currentScreen)
		{
		case arkanoid::screens::MenuScreen:

			menu::inputs();
			break;

		case arkanoid::screens::GameplayScreen:

			gameplay::inputs();
			break;

		case arkanoid::screens::CreditsScreen:

			credits::inputs();
			break;

		case arkanoid::screens::InstructionsScreen:

			instructions::inputs();
			break;

		case arkanoid::screens::ResultsScreen:

			results::inputs();
			break;
		}
	}
	static void update()
	{
		switch (currentScreen)
		{
		case arkanoid::screens::MenuScreen:

			menu::update();
			break;

		case arkanoid::screens::GameplayScreen:

			gameplay::update();
			break;

		case arkanoid::screens::CreditsScreen:

			credits::update();
			break;

		case arkanoid::screens::InstructionsScreen:

			instructions::update();
			break;

		case arkanoid::screens::ResultsScreen:

			results::update();
			break;
		}
	}
	static void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);

		switch (currentScreen)
		{
		case arkanoid::screens::MenuScreen:

			menu::draw();
			break;

		case arkanoid::screens::GameplayScreen:

			gameplay::draw();
			break;

		case arkanoid::screens::CreditsScreen:

			credits::draw();
			break;

		case arkanoid::screens::InstructionsScreen:

			instructions::draw();
			break;

		case arkanoid::screens::ResultsScreen:

			results::draw();
			break;
		}

		EndDrawing();
	}
	static void deinit()
	{
		menu::deinit();
		gameplay::deinit();
		credits::deinit();
		instructions::deinit();
		results::deinit();
		CloseAudioDevice();
		CloseWindow();
	}
	void run()
	{
		arkanoid::init();
		while (!WindowShouldClose())
		{
			arkanoid::inputs();
			arkanoid::update();
			arkanoid::draw();
		}
		arkanoid::deinit();
	}
}