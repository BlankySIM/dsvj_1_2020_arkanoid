#ifndef GAME_H
#define GAME_H
#include "raylib.h"
#include "scenes/credits/credits.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/instructions/instructions.h"
#include "scenes/menu/menu.h"
#include "scenes/results/results.h"

namespace arkanoid
{
	void run();
}
#endif

