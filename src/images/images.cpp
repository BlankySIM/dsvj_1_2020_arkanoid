#include "images.h"

namespace arkanoid
{
	namespace images
	{
		Image buildings = LoadImage("res/assets/buildings_layer.png");
		Texture2D longButton;
		Texture2D brick;
		Texture2D unbreakableBrick;
		Texture2D lifesContainer;
		Texture2D ball;
		Texture2D playerPad;
		Texture2D shortButton;
		Texture2D shortContainer;
		Texture2D buildingsTexture;
		Texture2D cityBackground;
		Texture2D cityForeground;
		const int buildingsNewWidth = 354;
		const int buildingsNewHeight = 258;

		void initBackgroundTextures()
		{
			ImageResize(&images::buildings, buildingsNewWidth, buildingsNewHeight);
			buildingsTexture = LoadTextureFromImage(buildings);
			longButton = LoadTexture("res/assets/long_button.png");
			cityBackground = LoadTexture("res/assets/city_background_layer.png");
			cityForeground = LoadTexture("res/assets/city_foreground_layer.png");
			brick = LoadTexture("res/assets/brick.png");
			unbreakableBrick = LoadTexture("res/assets/brick_unbreakable.png");
			lifesContainer = LoadTexture("res/assets/lifes_container.png");
			ball = LoadTexture("res/assets/ball.png");
			playerPad = LoadTexture("res/assets/player_pad.png");
			shortButton = LoadTexture("res/assets/short_button.png");
			shortContainer = LoadTexture("res/assets/short_container.png");
		}
		void drawCommonBackground()
		{
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, SKYBLUE);
			DrawTexture(buildingsTexture, screenZero, screenZero + 150, WHITE);
			DrawTexture(buildingsTexture, middleScreenW, screenZero + 180, WHITE);
			DrawTexture(cityBackground, screenZero, screenZero, WHITE);
			DrawTexture(cityForeground, screenZero, screenZero, WHITE);
		}
		void drawChangingBackground(Color backgroundColor)
		{
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, backgroundColor);
			DrawTexture(buildingsTexture, screenZero, screenZero + 150, WHITE);
			DrawTexture(buildingsTexture, middleScreenW, screenZero + 180, WHITE);
			DrawTexture(cityBackground, screenZero, screenZero, WHITE);
			DrawTexture(cityForeground, screenZero, screenZero, BLACK);
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, Fade(backgroundColor, 0.4f));
		}
		void deinit()
		{
			UnloadImage(buildings);
			UnloadTexture(longButton);
			UnloadTexture(cityBackground);
			UnloadTexture(cityForeground);
			UnloadTexture(brick);
			UnloadTexture(unbreakableBrick);
			UnloadTexture(lifesContainer);
			UnloadTexture(ball);
			UnloadTexture(playerPad);
			UnloadTexture(shortButton);
			UnloadTexture(shortContainer);
			UnloadTexture(buildingsTexture);
		}
	}
}