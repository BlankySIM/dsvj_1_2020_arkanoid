#ifndef IMAGES_H
#define IMAGES_H
#include "raylib.h"
#include "screens/screens.h"

using namespace arkanoid::screens;

namespace arkanoid
{
	namespace images
	{
		extern Texture2D longButton;
		extern Texture2D brick;
		extern Texture2D unbreakableBrick;
		extern Texture2D lifesContainer;
		extern Texture2D ball;
		extern Texture2D playerPad;
		extern Texture2D shortButton;
		extern Texture2D shortContainer;
		extern Texture2D buildingsTexture;
		extern Texture2D cityBackground;
		extern Texture2D cityForeground;

		void initBackgroundTextures();
		void drawCommonBackground();
		void drawChangingBackground(Color backgroundColor);
		void deinit();
	}
}
#endif

