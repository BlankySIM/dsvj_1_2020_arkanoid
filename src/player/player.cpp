#include "player.h"

namespace arkanoid
{
	namespace player
	{
		float playerDefaultX = static_cast<float>(screens::middleScreenW);
		float playerDefaultY = static_cast<float>(screens::screenHeight-75);
		const int padW = 150;
		const int padH = 15;
		const int halfPadW = padW / 2;
		const int halfPadH = padH / 2;
		const int playerLeftKey = KEY_A;
		const int playerRightKey = KEY_D;
		const int playersDefSpeed = 200;
		const int playersDefLifes = 4;
		const int firstGoal = 1000;
		const int speedMultiplier = 90;
		const int lifeRectangleSize = 18;
		int lifePoints;

		void init()
		{
			arkanoidPlayer = { playersDefLifes, playerDefaultX, playerDefaultY, padW, padH,
			false, WHITE, playerLeftKey, playerRightKey, playersDefSpeed, true, 0 };
			lifePoints = firstGoal;
		}
		void reInit()
		{
			arkanoidPlayer.lifes = playersDefLifes;
			arkanoidPlayer.position.x = playerDefaultX;
			arkanoidPlayer.position.y = playerDefaultY;
			arkanoidPlayer.holdingBall = true;
			arkanoidPlayer.win = false;
			lifePoints = firstGoal;
		}
		void inputs()
		{
			if (IsKeyDown(arkanoidPlayer.leftKey) && arkanoidPlayer.position.x >= screens::screenZero + halfPadW)
			{
				arkanoidPlayer.position.x -= arkanoidPlayer.speed * GetFrameTime();
			}
			if (IsKeyDown(arkanoidPlayer.rightKey) && arkanoidPlayer.position.x <= screens::screenWidth - halfPadW)
			{
				arkanoidPlayer.position.x += arkanoidPlayer.speed * GetFrameTime();
			}
			if (arkanoidPlayer.holdingBall)
			{
				if (IsKeyPressed(KEY_W))
				{
					arkanoidPlayer.holdingBall = false;
					ball::arkanoidBall.ballSpeed.y = ball::initialBallSpeed;
				}
			}
		}
		void lifeLose()
		{
			if (ball::arkanoidBall.ballPosition.y + ball::arkanoidBall.ballRadius >= screens::screenHeight)
			{
				arkanoidPlayer.holdingBall = true;
				ball::arkanoidBall.ballSpeed.x = 0;
				ball::arkanoidBall.ballSpeed.y = ball::initialBallSpeed;
				arkanoidPlayer.lifes--;
			}
		}
		void holdBall()
		{
			ball::arkanoidBall.ballSpeed.x = 0;
			ball::arkanoidBall.ballPosition.x = arkanoidPlayer.position.x;
			ball::arkanoidBall.ballPosition.y = arkanoidPlayer.position.y - halfPadH - 15;
		}
		void ballCollision()
		{
			if (ball::arkanoidBall.ballPosition.y + ball::arkanoidBall.ballRadius >= arkanoidPlayer.position.y - halfPadH
				&& ball::arkanoidBall.ballPosition.x >= arkanoidPlayer.position.x - halfPadW
				&& ball::arkanoidBall.ballPosition.x <= arkanoidPlayer.position.x + halfPadW
				&& ball::arkanoidBall.ballPosition.y + ball::arkanoidBall.ballRadius <= arkanoidPlayer.position.y + halfPadH
				&& ball::arkanoidBall.ballSpeed.y > 0)
			{
				ball::arkanoidBall.ballSpeed.y *= -1.0f;
				ball::arkanoidBall.ballSpeed.x = ((ball::arkanoidBall.ballPosition.x - arkanoidPlayer.position.x) / (halfPadW) * 5) * speedMultiplier;
			}
			else if (ball::arkanoidBall.ballPosition.x + ball::arkanoidBall.ballRadius >= arkanoidPlayer.position.x - halfPadW
				&& ball::arkanoidBall.ballPosition.x + ball::arkanoidBall.ballRadius <= arkanoidPlayer.position.x - halfPadW + 5
				&& ball::arkanoidBall.ballPosition.y >= arkanoidPlayer.position.y - halfPadH
				&& ball::arkanoidBall.ballPosition.y <= arkanoidPlayer.position.y + halfPadH && ball::arkanoidBall.ballSpeed.x > 0)
			{
				ball::arkanoidBall.ballSpeed.x *= -1.0f;
			}
			else if (ball::arkanoidBall.ballPosition.x - ball::arkanoidBall.ballRadius <= arkanoidPlayer.position.x + halfPadW
				&& ball::arkanoidBall.ballPosition.x - ball::arkanoidBall.ballRadius >= arkanoidPlayer.position.x + halfPadW - 5
				&& ball::arkanoidBall.ballPosition.y >= arkanoidPlayer.position.y - halfPadH
				&& ball::arkanoidBall.ballPosition.y <= arkanoidPlayer.position.y + halfPadH && ball::arkanoidBall.ballSpeed.x < 0)
			{
				ball::arkanoidBall.ballSpeed.x *= -1.0f;
			}
		}
		void draw()
		{
			DrawTexture(images::playerPad, static_cast<int>(arkanoidPlayer.position.x) - player::halfPadW, static_cast<int>(arkanoidPlayer.position.y) - player::halfPadH, WHITE);
			#if DEBUG
			DrawRectangleLines(static_cast<int>(arkanoidPlayer.position.x) - player::halfPadW, static_cast<int>(arkanoidPlayer.position.y) - player::halfPadH, arkanoidPlayer.padW, arkanoidPlayer.padH, GREEN);
			#endif
		}
		void drawLifes()
		{
			DrawTexture(images::lifesContainer, screens::screenZero + 15, screens::screenHeight - 55, WHITE);
			for (int i = 0; i < arkanoidPlayer.lifes; i++)
			{
				DrawRectangle((i * (lifeRectangleSize + 2)) + (screens::screenZero + 60), (screens::screenHeight - 40), lifeRectangleSize, lifeRectangleSize, RED);
			}
		}
		void drawPoints()
		{
			DrawText(FormatText("Score: %04i", arkanoidPlayer.points), screens::middleScreenW - 90, screens::screenZero + 10, screens::medFontSize, WHITE);
		}
		void winLife()
		{
			if (arkanoidPlayer.points >= lifePoints)
			{			
				if (arkanoidPlayer.lifes < playersDefLifes)
				{
					arkanoidPlayer.lifes++;
				}
				lifePoints += firstGoal;
			}
		}

		PLAYER arkanoidPlayer = { 0 };
	}
}