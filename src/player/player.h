#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "screens/screens.h"
#include "images/images.h"
#include "ball/ball.h"

namespace arkanoid
{
	namespace player
	{
		struct PLAYER
		{
			int lifes;
			Vector2 position;
			int padW;
			int padH;
			bool win;
			Color padColor;
			int leftKey;
			int rightKey;
			int speed;
			bool holdingBall;
			int points;
		};

		void init();
		void reInit();
		void inputs();
		void lifeLose();
		void holdBall();
		void ballCollision();
		void draw();
		void drawLifes();
		void drawPoints();
		void winLife();

		extern PLAYER arkanoidPlayer;
	}
}
#endif
