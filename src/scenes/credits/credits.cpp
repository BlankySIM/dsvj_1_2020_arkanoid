#include "credits.h"

namespace arkanoid
{
	namespace credits
	{
		int key;
		int currentPage;
		const int minusc = 96;
		const int mayusc = 32;
		const int firstPage = 0;
		const int secondPage = 1;

		void init()
		{
			currentPage = firstPage;
		}
		void inputs()
		{
			key = GetKeyPressed();
		}
		void update()
		{
			UpdateMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::cityAmbiance);
			if (key > minusc)
			{
				key -= mayusc;
			}
			if (key==KEY_M)
			{
				currentScreen = MenuScreen;
				PlaySound(audio::bricksfx);
			}
			switch (key)
			{
			case KEY_M:

				currentScreen = MenuScreen;
				currentPage = firstPage;
				PlaySound(audio::bricksfx);
				break;

			case KEY_P:

				if (currentPage == firstPage)
				{
					currentPage = secondPage;
					PlaySound(audio::bricksfx);
				}
				else
				{
					currentPage = firstPage;
					PlaySound(audio::bricksfx);
				}
				break;
			}
		}
		void draw()
		{
			images::drawCommonBackground();
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, Fade(BLACK, 0.6f));
			DrawText("Credits", middleScreenW - 70, screenZero + 10, maxFontSize, WHITE);
			DrawTexture(images::shortButton, screenZero + 15, screens::screenHeight - 52, WHITE);
			DrawText("M  Menu", screenZero + 28, screenHeight - 42, minFontSize, WHITE);
			DrawTexture(images::shortButton, screenWidth - 150, screens::screenHeight - 52, WHITE);
			if (currentPage == 0)
			{			
				DrawText("Game Dev: ", screenZero + 10, screenZero + 90, medFontSize, WHITE);
				DrawText("-  Blanco Juan Simon: blancojuansimon@gmail.com", screenZero + 10, screenZero + 130, minFontSize, WHITE);
				DrawText("-  Game created with raylib library: https://www.raylib.com", screenZero + 10, screenZero + 160, minFontSize, WHITE);
				DrawText("Game Art:", screenZero + 10, screenZero + 200, medFontSize, WHITE);
				DrawText("Ruined City Background:", screenZero + 10, screenZero + 240, minFontSize, WHITE);
				DrawText("-  TokyoGeisha: https://opengameart.org/users/tokyogeisha", screenZero + 10, screenZero + 270, minFontSize, WHITE);
				DrawText("-  License: https://creativecommons.org/publicdomain/zero/1.0/", screenZero + 10, screenZero + 300, minFontSize, WHITE);
				DrawText("Urban Landscape:", screenZero + 10, screenZero + 330, minFontSize, WHITE);
				DrawText("-  ansimuz: https://opengameart.org/users/ansimuz", screenZero + 10, screenZero + 360, minFontSize, WHITE);
				DrawText("-  License: https://creativecommons.org/publicdomain/zero/1.0/", screenZero + 10, screenZero + 390, minFontSize, WHITE);
				DrawText("UI/Gameplay elements", screenZero + 10, screenZero + 430, minFontSize, WHITE);
				DrawText("-  Blanco Juan Simon: blancojuansimon@gmail.com", screenZero + 10, screenZero + 460, minFontSize, WHITE);
				DrawText("P  Next", screenWidth - 137, screenHeight - 42, minFontSize, WHITE);
			}
			else
			{
				DrawText("Game Audio: ", screenZero + 10, screenZero + 90, medFontSize, WHITE);
				DrawText("Jazzy Vibes #36 - Loop - Smooth Jazz", screenZero + 10, screenZero + 130, minFontSize, WHITE);
				DrawText("-  Ellary: opengameart.org/users/ellary", screenZero + 10, screenZero + 160, minFontSize, WHITE);
				DrawText("-  License: https://creativecommons.org/licenses/by/4.0/", screenZero + 10, screenZero + 190, minFontSize, WHITE);
				DrawText("Ring", screenZero + 10, screenZero + 220, minFontSize, WHITE);
				DrawText("-  LukeUPF: https://freesound.org/people/LukeUPF/", screenZero + 10, screenZero + 250, minFontSize, WHITE);
				DrawText("-  License: https://creativecommons.org/licenses/by/4.0/", screenZero + 10, screenZero + 280, minFontSize, WHITE);
				DrawText("Gameplay music/ Brick break SFX/ City ambiance SFX", screenZero + 10, screenZero + 310, minFontSize, WHITE);
				DrawText("-  https://freesfx.co.uk/", screenZero + 10, screenZero + 340, minFontSize, WHITE);
				DrawText("P  Back", screenWidth - 137, screenHeight - 42, minFontSize, WHITE);
			}
		}
		void deinit()
		{

		}
	}
}