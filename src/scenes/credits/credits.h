#ifndef CREDITS_H
#define	CREDITS_H
#include "raylib.h"
#include "screens/screens.h"
#include "audio/audio.h"
#include "images/images.h"

using namespace arkanoid::screens;

namespace arkanoid
{
	namespace credits
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
