#include "gameplay.h"

namespace arkanoid
{
	namespace gameplay
	{
		bool pause = false;
		bool goToMenu = false;
		const int day = 4;
		const int sunset = 7;
		const int night = 10;
		const int midnight = 13;
		const int earlyMorning = 16;
		const int dawn = 19;
		const int controlsRecW = 170;
		const int controlsRecH = 100;
		const int vShapeLevels = 5;
		const int previousLevel = 2;
		const int lastColumnLevel = 4;
		const int minLifes = 1;
		Color backgroundColor;

		void init()
		{
			ball::init();
			backgroundColor = SKYBLUE;
			player::init();
			brick::init();
		}
		static void debugLevelChange()
		{
			if (currentLevel!=lastLevel)
			{
				if (IsKeyPressed(KEY_ZERO))
				{
					currentLevel++;
					player::reInit();
					brick::init();
				}
			}
			if (currentLevel != firstLevel)
			{
				if (IsKeyPressed(KEY_NINE))
				{
					currentLevel--;
					if (brick::levelCounter==firstLevel)
					{
						brick::levelCounter = lastColumnLevel;
					}
					else
					{
						brick::levelCounter -= previousLevel;
					}
					if (brick::levelCounter<=0)
					{
						brick::levelCounter = vShapeLevels;
					}
					player::reInit();
					brick::init();
				}
			}
		}
		void inputs()
		{
			if (IsKeyPressed(KEY_P))
			{				
				if (pause == true)
				{
					PlaySound(audio::bricksfx);
					pause = false;
				}
				else if (pause == false)
				{
					PlaySound(audio::buttonsfx);
					pause = true;
				}
			}
			if (!pause)
			{
				player::inputs();
				#if DEBUG
				debugLevelChange();
				#endif
			}
			else
			{
				if (IsKeyPressed(KEY_M))
				{
					goToMenu = true;
				}
				audio::muteInput();
			}
		}
		static void changeBackgroundColor()
		{
			if (currentLevel < day)
			{
				backgroundColor = SKYBLUE;
			}
			else if (currentLevel < sunset)
			{
				backgroundColor = ORANGE;
			}
			else if (currentLevel < night)
			{
				backgroundColor = DARKPURPLE;
			}
			else if (currentLevel < midnight)
			{
				backgroundColor = DARKBLUE;
			}
			else if (currentLevel < earlyMorning)
			{
				backgroundColor = PINK;
			}
			else if (currentLevel < dawn)
			{
				backgroundColor = ORANGE;
			}
			else
			{
				backgroundColor = SKYBLUE;
			}
		}
		static void drawPlayerControls()
		{
			if (player::arkanoidPlayer.holdingBall)
			{
				DrawText("A <-", static_cast<int>(player::arkanoidPlayer.position.x - 170), static_cast<int>(player::arkanoidPlayer.position.y - 60), medFontSize, WHITE);
				DrawText("-> D", static_cast<int>(player::arkanoidPlayer.position.x + 110), static_cast<int>(player::arkanoidPlayer.position.y - 60), medFontSize, WHITE);
				DrawTexture(images::shortButton, static_cast<int>(player::arkanoidPlayer.position.x - 55), static_cast<int>(screens::middleScreenH + 60), WHITE);
				DrawText("W  Start", static_cast<int>(player::arkanoidPlayer.position.x - 41), static_cast<int>(screens::middleScreenH + 70), minFontSize, WHITE);
			}
		}
		static void drawDebugControls()
		{
			DrawRectangle(screenWidth - 170, middleScreenH - 20, controlsRecW, controlsRecH, Fade(BLACK, 0.8f));
			DrawText("Debug", screenWidth - 115, middleScreenH - 15, minFontSize, WHITE);
			if (currentLevel!=lastLevel)
			{
				DrawText("0 Next Level", screenWidth - 150, middleScreenH + 45, minFontSize, WHITE);
			}
			if (currentLevel!=firstLevel)
			{
				DrawText("9 Prev Level", screenWidth - 150, middleScreenH + 10, minFontSize, WHITE);
			}
		}
		void update()
		{
			audio::muteButtonUpdate();
			PlayMusicStream(audio::gameplayMusic);
			PlayMusicStream(audio::cityAmbiance);
			UpdateMusicStream(audio::gameplayMusic);
			UpdateMusicStream(audio::cityAmbiance);
			if (!pause)
			{
				if (!audio::muted)
				{
					SetMusicVolume(audio::gameplayMusic, audio::gameplayVolume);
				}

				if (player::arkanoidPlayer.holdingBall)
				{
					player::holdBall();
				}
				else
				{
					ball::ballbehavior();
				}
				player::ballCollision();
				player::lifeLose();
				player::winLife();
				brick::ballCollision();

				if (player::arkanoidPlayer.lifes < minLifes)
				{
					player::arkanoidPlayer.win = false;
					currentScreen = ResultsScreen;
				}
				if (brick::bricksToWin == 0)
				{
					player::arkanoidPlayer.win = true;
					currentScreen = ResultsScreen;
				}
			}
			else
			{
				if (!audio::muted)
				{
					SetMusicVolume(audio::gameplayMusic, audio::gameplayPauseVolume);
				}

				if (goToMenu)
				{
					PlaySound(audio::bricksfx);
					goToMenu = false;
					pause = false;
					currentScreen = MenuScreen;
					currentLevel = firstLevel;
					player::reInit();
					player::arkanoidPlayer.points = 0;
					brick::init();
					StopMusicStream(audio::gameplayMusic);
				}
			}
			changeBackgroundColor();
		}
		void draw()
		{
			images::drawChangingBackground(backgroundColor);
			ball::draw();			
			brick::draw();
			player::draw();
			player::drawLifes();	
			player::drawPoints();
			drawPlayerControls();
			DrawTexture(images::shortContainer, middleScreenW - 215, screenHeight - 52, WHITE);
			DrawText(FormatText("Lv: %0i", currentLevel), middleScreenW - 200, screenHeight - 42, screens::minFontSize, WHITE);
			#if DEBUG
			DrawText(FormatText("To win: %0i", brick::bricksToWin), screens::middleScreenW - 90, screens::middleScreenH, screens::medFontSize, WHITE);
			drawDebugControls();
			#endif
			if (pause)
			{
				DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, Fade(BLACK, 0.8f));
				DrawTexture(images::longButton, middleScreenW - 130, middleScreenH - 75, WHITE);
				DrawTexture(images::longButton, middleScreenW - 130, middleScreenH, WHITE);
				DrawText("Pause", middleScreenW - 60, screenZero + 80, maxFontSize, WHITE);
				DrawText("M  Go to menu", middleScreenW - 111, middleScreenH - 59, medFontSize, WHITE);
				DrawText("P   Continue", middleScreenW - 110, middleScreenH + 15, medFontSize, WHITE);
				DrawText("Press ESC to exit", screenWidth - 200, screenHeight - 30, minFontSize, WHITE);
				audio::drawMuteButton();
			}
			else
			{
				DrawTexture(images::shortButton, screens::middleScreenW - 60, screens::screenHeight - 52, WHITE);
				DrawText("P  Pause", middleScreenW - 46, screenHeight - 42, minFontSize, WHITE);
			}
		}
		void deinit()
		{
			UnloadMusicStream(audio::gameplayMusic);
			UnloadSound(audio::bricksfx);
			brick::deinit();
		}
	}
}