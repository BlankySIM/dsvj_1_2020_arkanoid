#ifndef GAMEPLAY_H
#define	GAMEPLAY_H
#include "raylib.h"
#include "ball/ball.h"
#include "player/player.h"
#include "brick/brick.h"
#include "audio/audio.h"

namespace arkanoid
{
	namespace gameplay
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
