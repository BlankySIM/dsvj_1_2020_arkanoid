#include "instructions.h"

namespace arkanoid
{
	namespace instructions
	{
		int key;
		int currentPage;
		const int minusc = 96;
		const int mayusc = 32;
		const int firstPage = 0;
		const int secondPage = 1;

		void init()
		{
			currentPage = firstPage;
		}
		void inputs()
		{
			key = GetKeyPressed();
		}
		void update()
		{
			UpdateMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::cityAmbiance);
			player::arkanoidPlayer.position.y = static_cast<float>(middleScreenH + 50);

			if (key > minusc)
			{
				key -= mayusc;
			}
			switch (key)
			{
			case KEY_M:

				currentScreen = MenuScreen;
				currentPage = firstPage;
				PlaySound(audio::bricksfx);
				player::reInit();
				break;

			case KEY_P:

				if (currentPage == firstPage)
				{
					currentPage = secondPage;
					PlaySound(audio::bricksfx);
				}
				else
				{
					currentPage = firstPage;
					PlaySound(audio::bricksfx);
				}
				break;
			}
		}
		void draw()
		{
			images::drawCommonBackground();
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, Fade(BLACK, 0.6f));
			DrawText("Instructions", middleScreenW - 125, screenZero + 10, maxFontSize, WHITE);
			DrawTexture(images::shortButton, screenZero + 15, screens::screenHeight - 52, WHITE);
			DrawText("M  Menu", screenZero + 28, screenHeight - 42, minFontSize, WHITE);
			DrawTexture(images::shortButton, screenWidth - 150, screens::screenHeight - 52, WHITE);
			if (currentPage == 0)
			{
				DrawText("Rules", screenZero + 10, screenZero + 90, medFontSize, WHITE);
				DrawText("Try to beat all the levels :D", screenZero + 10, screenZero + 130, medFontSize, WHITE);
				DrawText("-  To win a level you have to destroy all the orange bricks.", screenZero + 10, screenZero + 170, minFontSize, WHITE);
				DrawText("-  If the ball hits the bottom of the screen you'll lose a life.", screenZero + 10, screenZero + 200, minFontSize, WHITE);
				DrawText("-  Lose 4 lifes and you'll have to start from the beginning.", screenZero + 10, screenZero + 230, minFontSize, WHITE);
				DrawText("-  You'll earn points for each life you carry to the end of a level.", screenZero + 10, screenZero + 260, minFontSize, WHITE);
				DrawText("-  The gray bricks won't break, but you don't need to break them to\n   win a level.", screenZero + 10, screenZero + 290, minFontSize, WHITE);
				DrawText("-  Each 1000 points you'll earn a life.", screenZero + 10, screenZero + 350, minFontSize, WHITE);

				DrawText("P  Next", screenWidth - 137, screenHeight - 42, minFontSize, WHITE);
			}
			else
			{
				DrawText("Controls", screenZero + 10, screenZero + 90, medFontSize, WHITE);
				player::draw();
				DrawText("W: Release Ball", middleScreenW - 120, middleScreenH - 40, medFontSize, WHITE);
				DrawText("A <-", static_cast<int>(player::arkanoidPlayer.position.x - 170), static_cast<int>(player::arkanoidPlayer.position.y - 15), medFontSize, WHITE);
				DrawText("-> D", static_cast<int>(player::arkanoidPlayer.position.x + 110), static_cast<int>(player::arkanoidPlayer.position.y - 15), medFontSize, WHITE);
				DrawText("P  Back", screenWidth - 137, screenHeight - 42, minFontSize, WHITE);
			}
		}
		void deinit()
		{

		}
	}
}