#ifndef INSTRUCTIONS_H
#define	INSTRUCTIONS_H
#include "raylib.h"
#include "images/images.h"
#include "audio/audio.h"
#include "player/player.h"

namespace arkanoid
{
	namespace instructions
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif