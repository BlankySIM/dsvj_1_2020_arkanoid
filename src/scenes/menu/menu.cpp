#include "menu.h"

namespace arkanoid
{
	namespace menu
	{
		int key;

		void init()
		{
			images::initBackgroundTextures();
			audio::setNormalVolumes();
		}
		void inputs()
		{
			key = GetKeyPressed();
			audio::muteInput();
		}
		void update()
		{
			audio::muteButtonUpdate();
			PlayMusicStream(audio::menuMusic);
			PlayMusicStream(audio::cityAmbiance);
			UpdateMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::cityAmbiance);

			switch (key)
			{
			case KEY_ONE:

				PlaySound(audio::buttonsfx);
				currentScreen = GameplayScreen;
				StopMusicStream(audio::menuMusic);
				break;

			case KEY_TWO:

				PlaySound(audio::buttonsfx);
				currentScreen = InstructionsScreen;
				break;

			case KEY_THREE:

				PlaySound(audio::buttonsfx);
				currentScreen = CreditsScreen;
				break;
			}
		}
		void draw()
		{
			images::drawCommonBackground();
			DrawText("Arkanoid", middleScreenW - 90, screenZero + 80, maxFontSize, WHITE);
			DrawText("Press ESC to exit", screenWidth - 200, screenHeight - 30, minFontSize, WHITE);
			DrawText("v1.0", screenZero + 15, screenHeight - 30, minFontSize, WHITE);
			DrawTexture(images::longButton, middleScreenW - 130, middleScreenH - 75, WHITE);
			DrawTexture(images::longButton, middleScreenW - 130, middleScreenH, WHITE);
			DrawTexture(images::longButton, middleScreenW - 130, middleScreenH + 75, WHITE);
			DrawText("1  Start Game", middleScreenW - 105, middleScreenH - 60, medFontSize, WHITE);
			DrawText("2 Instructions", middleScreenW - 109, middleScreenH + 15, medFontSize, WHITE);
			DrawText("3   Credits", middleScreenW - 109, middleScreenH + 90, medFontSize, WHITE);
			audio::drawMuteButton();
		}
		void deinit()
		{
			images::deinit();
			UnloadMusicStream(audio::menuMusic);
			UnloadMusicStream(audio::cityAmbiance);
			UnloadSound(audio::buttonsfx);
		}
	}
}