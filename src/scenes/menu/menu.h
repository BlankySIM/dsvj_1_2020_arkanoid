#ifndef MENU_H
#define	MENU_H
#include "raylib.h"
#include "screens/screens.h"
#include "audio/audio.h"
#include "images/images.h"

using namespace arkanoid::screens;

namespace arkanoid
{
	namespace menu
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif