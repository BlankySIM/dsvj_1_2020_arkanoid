#include "results.h"

namespace arkanoid
{
	namespace results
	{
		int key;
		const int minusc = 96;
		const int mayusc = 32;
		const int lifePoints = 150;

		void init()
		{

		}
		void inputs()
		{
			key = GetKeyPressed();
		}
		static void winPoints()
		{
			for (int i = 0; i < player::arkanoidPlayer.lifes; i++)
			{
				player::arkanoidPlayer.points += lifePoints;
			}
		}
		void update()
		{
			if (key > minusc)
			{
				key -= mayusc;
			}
			switch (key)
			{
			case KEY_ONE:

				if (player::arkanoidPlayer.win)
				{
					if (currentLevel == lastLevel)
					{
						player::arkanoidPlayer.points = 0;
						currentLevel = firstLevel;
					}
					else
					{
						winPoints();
						currentLevel++;
					}
				}
				else
				{
					player::arkanoidPlayer.points = 0;
					currentLevel = firstLevel;
				}
				currentScreen = GameplayScreen;
				player::reInit();
				brick::init();
				break;

			case KEY_M:

				currentScreen = MenuScreen;
				currentLevel = firstLevel;
				player::reInit();
				brick::init();
				player::arkanoidPlayer.points = 0;
				break;
			}
		}
		void draw()
		{
			images::drawCommonBackground();
			DrawRectangle(screenZero, screenZero, screenWidth, screenHeight, Fade(BLACK, 0.6f));
			DrawTexture(images::longButton, middleScreenW - 130, middleScreenH - 75, WHITE);
			DrawTexture(images::longButton, middleScreenW - 130, middleScreenH, WHITE);
			if (player::arkanoidPlayer.win)
			{
				if (currentLevel == lastLevel)
				{
					DrawText("Congrats!", middleScreenW - 85, screenZero + 80, maxFontSize, WHITE);
					DrawText("You beated all the levels!", middleScreenW - 250, screenZero + 130, maxFontSize, WHITE);
					DrawText("1  Start again", middleScreenW - 106, middleScreenH - 59, medFontSize, WHITE);
				}
				else
				{
					DrawText("You won!", middleScreenW - 85, screenZero + 80, maxFontSize, WHITE);
					DrawText("1  Next level", middleScreenW - 106, middleScreenH - 59, medFontSize, WHITE);
				}
			}
			else
			{
				DrawText("You lost", middleScreenW - 85, screenZero + 80, maxFontSize, WHITE);
				DrawText("1  Try again", middleScreenW - 106, middleScreenH - 59, medFontSize, WHITE);
			}
			player::drawPoints();
			DrawText("M Go to menu", middleScreenW - 110, middleScreenH + 15, medFontSize, WHITE);
			DrawText("Press ESC to exit", screenWidth - 200, screenHeight - 30, minFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}