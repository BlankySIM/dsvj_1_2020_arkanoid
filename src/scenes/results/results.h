#ifndef RESULTS_H
#define	RESULTS_H
#include "raylib.h"
#include "player/player.h"
#include "brick/brick.h"
#include "screens/screens.h"

using namespace arkanoid::screens;

namespace arkanoid
{
	namespace results
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif
