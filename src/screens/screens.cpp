#include "screens.h"

namespace arkanoid
{
	namespace screens
	{
		const int  screenZero = 0;
		const int  minFontSize = 20;
		const int  medFontSize = 30;
		const int  maxFontSize = 40;
		const int screenWidth = 800;
		const int screenHeight = 660;
		const int middleScreenW = screenWidth / 2;
		const int middleScreenH = screenHeight/2;
		int currentLevel = firstLevel;

		GameState currentScreen = MenuScreen;
	}
}