#ifndef SCREENS_H
#define	SCREENS_H
#include "raylib.h"

namespace arkanoid
{
	namespace screens
	{
		const int firstLevel = 1;
		const int lastLevel = 20;
		extern const int screenZero;
		extern const int minFontSize;
		extern const int medFontSize;
		extern const int maxFontSize;
		extern const int screenWidth;
		extern const int screenHeight;
		extern const int middleScreenW;
		extern const int middleScreenH;
		extern int currentLevel;

		enum GameState
		{
			MenuScreen,
			GameplayScreen,
			CreditsScreen,
			InstructionsScreen,
			ResultsScreen,
		};

		extern GameState currentScreen;
	}
}
#endif
